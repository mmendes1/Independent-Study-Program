import java.util.Random;

public class Matrix extends MatrixCalc {

    private int size, matrixNum;
    private int[][] matrix;

    public Matrix () {}

    public Matrix (int size) {
        int[][] matrix = new int[size][size];
            this.size = size;
            this.matrix = matrix;
            matrixNum = 1;
    }

    public int[][] basicMult(Matrix A, Matrix B) {
        return super.basicMult(A.getMatrix(), B.getMatrix());
    }

    public int[][] strassenMult(Matrix A, Matrix B) {
        return super.strassenMult(A.getMatrix(), B.getMatrix());
    }

    public int[][] createMatrix() {
        Random rand = new Random();
        if(size > 4) { return null; }
        //size += 2;
        matrix = new int[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int random = rand.nextInt(100 + 101) - 100;
                matrix[i][j] = size;//change to random when using
            }
        }
        if (!checkSize(size, 2)) { matrix = appendMatrix(matrix, size); }
        printMatrix(matrix, "Matrix " + matrixNum, size);
            matrixNum++;
            return matrix;
    }

    public int[][] getMatrix() { return matrix; }

    boolean checkSize(int n, int pow)
    {
        if(n==0)
            return false;
        return (int)(Math.ceil((Math.log(n) / Math.log(pow)))) ==
                (int)(Math.floor(((Math.log(n) / Math.log(pow)))));
    }

    int[][] appendMatrix(int[][] matrix, int size) {
        int count = 0;
        int newSize = 2, comp, power;
        int[] powArr = new int[7];
        powArr[0] = Math.abs(2 - size);
        powArr[1] = Math.abs(4 - size);
        powArr[2] = Math.abs(8 - size);
        powArr[3] = Math.abs(16 - size);
        powArr[4] = Math.abs(32 - size);
        powArr[5] = Math.abs(64 - size);
        powArr[6] = Math.abs(128 - size);
        comp = powArr[0];
        while (count < powArr.length) {
            if (powArr[count] < comp) {
                comp = powArr[count];
                power = count + 1;
                newSize = (int)Math.pow(2.0, (double)count + 1);
            }
            //System.out.println("Val: " + powArr[count] +" Count: " + count + "\nNew Size: " + newSize);
            count++;
        }
        if (size > newSize) newSize *= 2;
        //System.out.println("Size: " + size + "\nNew Size: " + newSize);

        int[][] append = new int [newSize][newSize];
        for (int i = 0; i < newSize; i++) {
            for (int j = 0; j < newSize; j++) {
                append[i][j] = 0;
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                append[i][j] = matrix[i][j];
            }
        }
        //printMatrix(append, "Appended Matrix: ", newSize);

        return append;
    }

    public void printMatrix(int[][] matrix, String matrixID, int size) {
        System.out.println(matrixID);
        System.out.println("-----------------------------------------------------------------------");
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println("");
        }
    }
}

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MatrixTest {

    private int size = 2;
    private int expected[][];


    @Test
    void testCreateMatrix() {
        Matrix test = new Matrix(2);
        test.createMatrix();
        expected = new int[2][2];
        for(int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                expected[i][j] = size;
            }
        }
        Assertions.assertAll(
                () -> assertEquals(expected[0][0], test.getMatrix()[0][0]),
                () -> assertEquals(expected[0][1], test.getMatrix()[0][1]),
                () -> assertEquals(expected[1][0], test.getMatrix()[1][0]),
                () -> assertEquals(expected[1][1], test.getMatrix()[1][1])
        );
    }

    @Test
    void testBasicMult() {
        Matrix test1 = new Matrix(2);
        test1.createMatrix();

        Matrix test2 = new Matrix(2);
        test2.createMatrix();

        expected = new int[2][2];
        for(int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                expected[i][j] = 8;
            }
        }

        int[][] result = test1.basicMult(test1, test2);
        Assertions.assertAll(
                () -> assertEquals(expected[0][0], result[0][0]),
                () -> assertEquals(expected[0][1], result[0][1]),
                () -> assertEquals(expected[1][0], result[1][0]),
                () -> assertEquals(expected[1][1], result[1][1])
        );
    }

    @Test
    void testLadermanMult() {
        Matrix test1 = new Matrix(2);
        test1.createMatrix();

        Matrix test2 = new Matrix(2);
        test2.createMatrix();

        expected = new int[2][2];
        for(int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                expected[i][j] = 8;
            }
        }
        int[][] result = test1.strassenMult(test1, test2);
        Assertions.assertAll(
                () -> assertEquals(expected[0][0], result[0][0]),
                () -> assertEquals(expected[0][1], result[0][1]),
                () -> assertEquals(expected[1][0], result[1][0]),
                () -> assertEquals(expected[1][1], result[1][1])
        );
    }


}
